package com.example.task_3_2.interfaces;

public interface TextFieldTypes {

    String LOGIN = "login";
    String EMAIL = "email";
    String PHONE_NUMBER = "phoneNumber";
    String PASSWORD = "password";
    String RETYPE_PASSWORD = "retypePassword";
}