package com.example.task_3_2.utils;

import android.widget.TextView;

import com.example.task_3_2.interfaces.TextFieldsValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator implements TextFieldsValidator {

    private Pattern pattern;
    private Matcher matcher;

    public Validator(){}

    //================================================================================//

    @Override
    public boolean isTextFieldEmpty(TextView textView) {
        return textView.getText().toString().trim().isEmpty();
    }

    @Override
    public boolean emailValidator(TextView textView) {
        String email = textView.getText().toString().trim();
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email.trim());

        return matcher.matches();
    }

    @Override
    public boolean phoneNumberValidator(TextView textView) {
        String phoneNumber = textView.getText().toString().trim();
        pattern = Pattern.compile(PHONE_NUMBER_PATTERN);
        matcher = pattern.matcher(phoneNumber.trim());

        return matcher.matches();
    }

    @Override
    public boolean compareTextInTextFields(TextView textViewFirst, TextView textViewSecond) {
        String textViewFirstString = textViewFirst.getText().toString().trim();
        String textViewSecondString = textViewSecond.getText().toString().trim();

        return textViewFirstString.equals(textViewSecondString);
    }
}

