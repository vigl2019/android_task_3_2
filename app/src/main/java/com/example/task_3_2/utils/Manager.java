package com.example.task_3_2.utils;

import android.widget.EditText;
import android.widget.TextView;

import com.example.task_3_2.R;
import com.example.task_3_2.interfaces.GetView;
import com.example.task_3_2.interfaces.TextFieldTypes;
import com.example.task_3_2.interfaces.TextFieldsValidator;

public class Manager {

    private GetView getView;

    //================================================================================//

    public Manager(GetView getView) {
        this.setGetView(getView);
    }

    public GetView getGetView() {
        return getView;
    }

    public void setGetView(GetView getView) {
        this.getView = getView;
    }

    //================================================================================//

    public boolean validateTextField(String textFieldType) {

        boolean isValidationSuccess = true;
        Validator validator = new Validator();
        TextView txtMessage = (TextView) this.getGetView().getViewById(R.id.txtMessage);

        switch (textFieldType.trim()) {

            case TextFieldTypes.LOGIN: {

                EditText txtLogin = (EditText) this.getGetView().getViewById(R.id.txtLogin);

                if (validator.isTextFieldEmpty(txtLogin)) {
                    txtMessage.setText(TextFieldsValidator.LOGIN_ERROR_MESSAGE);
                    txtLogin.requestFocus();
                    isValidationSuccess = false;
                }

                break;
            }

            case TextFieldTypes.EMAIL: {

                EditText txtEmail = (EditText) this.getGetView().getViewById(R.id.txtEmail);

                if (validator.isTextFieldEmpty(txtEmail)) {
                    txtMessage.setText(TextFieldsValidator.EMAIL_ERROR_MESSAGE);
                    txtEmail.requestFocus();
                    isValidationSuccess = false;

                    break;
                }

                if (!validator.emailValidator(txtEmail)) {
                    txtMessage.setText(TextFieldsValidator.EMAIL_VALID_ERROR_MESSAGE);
                    txtEmail.requestFocus();
                    isValidationSuccess = false;

                    break;
                }
            }

            case TextFieldTypes.PHONE_NUMBER: {

                EditText txtPhone = (EditText) this.getGetView().getViewById(R.id.txtPhone);

                if (validator.isTextFieldEmpty(txtPhone)) {
                    txtMessage.setText(TextFieldsValidator.PHONE_NUMBER_ERROR_MESSAGE);
                    txtPhone.requestFocus();
                    isValidationSuccess = false;

                    break;
                }

                if (!validator.phoneNumberValidator(txtPhone)) {
                    txtMessage.setText(TextFieldsValidator.PHONE_NUMBER_VALID_ERROR_MESSAGE);
                    txtPhone.requestFocus();
                    isValidationSuccess = false;

                    break;
                }
            }

            case TextFieldTypes.PASSWORD: {

                EditText txtPassword = (EditText) this.getGetView().getViewById(R.id.txtPassword);

                if (validator.isTextFieldEmpty(txtPassword)) {
                    txtMessage.setText(TextFieldsValidator.PASSWORD_ERROR_MESSAGE);
                    txtPassword.requestFocus();
                    isValidationSuccess = false;
                }

                break;
            }

            case TextFieldTypes.RETYPE_PASSWORD: {

                EditText txtPassword = (EditText) this.getGetView().getViewById(R.id.txtPassword);
                EditText txtRetypePassword = (EditText) this.getGetView().getViewById(R.id.txtRetypePassword);

                if (validator.isTextFieldEmpty(txtRetypePassword)) {
                    txtMessage.setText(TextFieldsValidator.PASSWORD_RETYPE_ERROR_MESSAGE);
                    txtRetypePassword.requestFocus();
                    isValidationSuccess = false;

                    break;
                }

                if (!validator.compareTextInTextFields(txtPassword, txtRetypePassword)) {
                    txtMessage.setText(TextFieldsValidator.PASSWORD_RETYPE_MATCH_ERROR_MESSAGE);
                    txtRetypePassword.requestFocus();
                    isValidationSuccess = false;

                    break;
                }
            }

            default:
                break;
        }

        return isValidationSuccess;
    }
}


