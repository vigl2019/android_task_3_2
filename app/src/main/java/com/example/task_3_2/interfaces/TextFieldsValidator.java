package com.example.task_3_2.interfaces;

import android.widget.TextView;

public interface TextFieldsValidator {

    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
            "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    String PHONE_NUMBER_PATTERN = "^\\+380[\\d]{9}$";

    String LOGIN_ERROR_MESSAGE = "Enter Login!";
    String EMAIL_ERROR_MESSAGE = "Enter Email!";
    String EMAIL_VALID_ERROR_MESSAGE = "Enter valid Email!";
    String PHONE_NUMBER_ERROR_MESSAGE = "Enter Phone Number!";
    String PHONE_NUMBER_VALID_ERROR_MESSAGE = "Enter valid Phone Number!";
    String PASSWORD_ERROR_MESSAGE = "Enter Password!";
    String PASSWORD_RETYPE_ERROR_MESSAGE = "Retype Password!";
    String PASSWORD_RETYPE_MATCH_ERROR_MESSAGE = "Passwords do not match!";

    String SUCCESS_VALIDATION_MASSAGE = "Validation Successful!";

    boolean isTextFieldEmpty(TextView textView);
    boolean emailValidator(TextView textView);
    boolean phoneNumberValidator(TextView textView);
    boolean compareTextInTextFields(TextView textViewOne, TextView textViewTwo);
}

