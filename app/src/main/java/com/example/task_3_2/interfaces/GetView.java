package com.example.task_3_2.interfaces;

import android.view.View;

public interface GetView {
    View getViewById(int id);
}
