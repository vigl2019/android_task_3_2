package com.example.task_3_2;

/*

2. Создать приложение с одним экраном.

5 полей для ввода (EditText), текстовое поле (TextView) и одну кнопку (Button).
Поля (логин, email, номер телефона, пароль, повторный пароль) разместить по центру, друг под другом, кнопку и текстовое поле - внизу экрана.

По нажатию на кнопку реализовать валидацию полей (должны быть заполнены все поля, пароли должны совпадать).
Уведомить пользователя о результате в текстовом поле ("валидация пройдена", "введите логин", "пароли не совпадают" пр.)

2.1* Проверять валидность email и номера телефона в международном формате (+380501234567)

*/

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.task_3_2.interfaces.GetView;
import com.example.task_3_2.interfaces.TextFieldTypes;
import com.example.task_3_2.interfaces.TextFieldsValidator;
import com.example.task_3_2.utils.Manager;

public class MainActivity extends AppCompatActivity {

    EditText txtLogin;
    EditText txtEmail;
    EditText txtPhone;
    EditText txtPassword;
    EditText txtRetypePassword;

    TextView txtMessage;
    Button btnSubmit;

    //================================================================================//

    private GetView getView = new GetView() {
        @Override
        public View getViewById(int id) {
            return findViewById(id);
        }
    };

    public GetView getGetView() {
        return getView;
    }

    public void setGetView(GetView getView) {
        this.getView = getView;
    }

    //================================================================================//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtLogin = findViewById(R.id.txtLogin);
        txtEmail = findViewById(R.id.txtEmail);
        txtPhone = findViewById(R.id.txtPhone);
        txtPassword = findViewById(R.id.txtPassword);
        txtRetypePassword = findViewById(R.id.txtRetypePassword);

        txtMessage = findViewById(R.id.txtMessage);
        btnSubmit = findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Manager manager = new Manager(getGetView());

                if (!manager.validateTextField(TextFieldTypes.LOGIN))
                    return;

                if (!manager.validateTextField(TextFieldTypes.EMAIL))
                    return;

                if (!manager.validateTextField(TextFieldTypes.PHONE_NUMBER))
                    return;

                if (!manager.validateTextField(TextFieldTypes.PASSWORD))
                    return;

                if (!manager.validateTextField(TextFieldTypes.RETYPE_PASSWORD))
                    return;

                txtMessage.setText(TextFieldsValidator.SUCCESS_VALIDATION_MASSAGE);
            }
        });
    }
}



